$(document).ready(function(){
  
  /*carousels*/
  $(".owl-reviews-carousel").owlCarousel({
      dots: true,
      items: 1,
      loop: true,
      mouseDrag: false
    }
  );
  $(".owl-blog-item-carousel").owlCarousel({
      dots: true,
      items: 1,
      loop: true,
      nav: true,
      navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    }
  );

});

