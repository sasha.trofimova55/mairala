window.onload = function () {
  var radius = 4.5, // set the radius of the circle
    circumference = 2 * radius * Math.PI;

  var els = document.querySelectorAll('circle');
  Array.prototype.forEach.call(els, function (el) {
    el.setAttribute('stroke-dasharray', circumference + 'em');
    el.setAttribute('r', radius + 'em');
  });

  document.querySelector('.radial-progress-center').setAttribute('r', (radius - 0.01 + 'em'));

}; 


